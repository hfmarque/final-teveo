from django import forms
from .models import Comment, Profile, Camera
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['chat_name']

class CameraIDForm(forms.Form):
    camera_id = forms.IntegerField(label="Enter Camera ID", min_value=0)

class CommentForm(forms.ModelForm):
    text = forms.CharField(widget=forms.Textarea(attrs={'rows': 4}), required=True)
    image_url = forms.URLField(required=False)  # Permitir a los usuarios especificar o modificar la URL de la imagen

    class Meta:
        model = Comment
        fields = ['camera', 'text', 'image_url']

    def clean_text(self):
        text = self.cleaned_data['text']
        if not text.strip():
            raise forms.ValidationError("El contenido del comentario no puede estar vacío.")
        return text 

class UserRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['password1', 'password2']