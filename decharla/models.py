from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    chat_name = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        # Si chat_name no está establecido, usar el nombre de usuario como valor predeterminado
        if not self.chat_name:
            self.chat_name = self.user.username
        super(Profile, self).save(*args, **kwargs)

class Camera(models.Model):
    identifier = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    location = models.CharField(max_length=100)
    image_url = models.URLField()

    def __str__(self):
        return self.name

class Comment(models.Model):
    camera = models.ForeignKey(Camera, on_delete=models.CASCADE, related_name='comments')
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    image_url = models.URLField(max_length=200, blank=True, null=True)  # Campo opcional para la URL de la imagen

    def __str__(self):
        return f'Comment on {self.camera.name} by {self.author.username} at {self.created_at}'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
