import requests
import xml.etree.ElementTree as ET
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.views import LoginView  # Importar la vista de inicio de sesión
from django.http import HttpResponseForbidden, HttpResponse
from django.contrib import messages
from .models import Camera, Comment, Profile
from .forms import UserRegistrationForm, CommentForm, ProfileForm, CameraIDForm
from django.utils.timezone import now
from django.utils import timezone
from django.db.models import Q  # Importa Q para consultas complejas
from django.urls import reverse
from django.core.paginator import Paginator
from django.http import JsonResponse

# Vista para mostrar todas las habitaciones disponibles
@login_required
def home(request):
    comments_list = Comment.objects.select_related('camera').order_by('-created_at')
    paginator = Paginator(comments_list, 10)  # Muestra 10 comentarios por página

    page_number = request.GET.get('page')
    comments = paginator.get_page(page_number)
    if request.method == 'POST':
        if 'logout' in request.POST:
            logout(request)
            return redirect('login')  # Redirige a la página de login después del logout
    return render(request, 'home.html', {'comments': comments})

@login_required
def configuration(request):
    try:
        profile = request.user.profile  # Obtener el perfil del usuario actual
    except Profile.DoesNotExist:
        # Si el perfil no existe, crearlo
        profile = Profile.objects.create(user=request.user)
    if request.method == 'POST':
        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return redirect('home')  # O cualquier otra página después de guardar la configuración
    else:
        form = ProfileForm(instance=profile)
    return render(request, 'configuration.html', {'form': form})

# Vista para mostrar la página de ayuda
@login_required
def help_page(request):
    return render(request, 'help.html')


@login_required
def camera_comments(request):
    if request.method == 'POST':
        form = CameraIDForm(request.POST)
        if form.is_valid():
            camera_id = form.cleaned_data['camera_id']
            return redirect('camera_detail', id=camera_id)
    else:
        form = CameraIDForm()

    return render(request, 'enter_camera_id.html', {
        'form': form
    })

def fetch_cameras():
    url = 'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml'
    response = requests.get(url)
    root = ET.fromstring(response.content)

    for camera in root.findall('camara'):
        identifier = camera.find('id').text
        name = camera.find('lugar').text
        location = camera.find('coordenadas').text
        image_url = camera.find('src').text

        # Check if camera already exists, if not create a new one
        obj, created = Camera.objects.update_or_create(
            identifier=identifier,
            defaults={'name': name, 'location': location, 'image_url': image_url}
        )

def fetch_cameras_listado2():
    url = 'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml'
    response = requests.get(url)
    root = ET.fromstring(response.content)

    for cam in root.findall('cam'):
        identifier = cam.get('id')
        image_url = cam.find('url').text
        name = cam.find('info').text
        latitude = cam.find('place/latitude').text
        longitude = cam.find('place/longitude').text
        location = f"Lat: {latitude}, Lon: {longitude}"

        # Check if camera already exists, if not create a new one
        obj, created = Camera.objects.update_or_create(
            identifier=identifier,
            defaults={'name': name, 'location': location, 'image_url': image_url}
        )

@login_required
def camera_list(request):
    fetch_cameras()  # Optionally call this function to update cameras
    fetch_cameras_listado2()  # Updates the camera data from Listado 2
    query = request.GET.get('q')
    if query:
        cameras = Camera.objects.filter(
            Q(identifier__icontains=query) | Q(name__icontains=query)
        )
    else:
        cameras = Camera.objects.all()

    return render(request, 'camera_list.html', {'cameras': cameras, 'query': query})

@login_required
def camera_image(request, id):
    camera = get_object_or_404(Camera, identifier=id)
    comments = Comment.objects.filter(camera=camera).order_by('-created_at')
    form = CommentForm()

    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.camera = camera
            comment.author = request.user
            comment.save()

            # Si es una solicitud HTMX, actualizar solo la parte de comentarios
            if "HX-Request" in request.headers:
                return render(request, 'comments_list.html', {'comments': comments, 'camera': camera})

            # Si no es HTMX, redirigir para recargar la página completa
            return redirect('camera_image', id=camera.identifier)
    
    context = {
        'camera': camera,
        'comments': comments,
        'form': form
    }

    # Si es una solicitud HTMX para la imagen, responder solo con la imagen actualizada
    if "HX-Request" in request.headers:
        print("Respondiendo a una solicitud HTMX para la imagen")  # Mensaje de depuración
        return HttpResponse(f'<img src="{camera.image_url}" class="card-img-top" alt="Camera Image" style="width:100%; height:auto;">')

    return render(request, 'camera_image.html', context)

@login_required
def get_comments_json(request, camera_id):
    comments = Comment.objects.filter(camera__identifier=camera_id).order_by('-created_at')
    comments_data = [{'author': c.author.username, 'text': c.text, 'created_at': c.created_at.strftime('%Y-%m-%d %H:%M:%S')} for c in comments]
    return JsonResponse(comments_data, safe=False)
    
@login_required
def camera_detail(request, id):
    camera = get_object_or_404(Camera, identifier=id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.camera = camera
            comment.author = request.user
            comment.save()
            return redirect('camera_detail', id=camera.identifier)
    else:
        form = CommentForm()
    comments = Comment.objects.filter(camera=camera).order_by('-created_at')
    return render(request, 'camera_detail.html', {'camera': camera, 'comments': comments, 'form': form})

# Vista para mostrar el panel de administrador
@login_required
def admin_panel(request):
    if request.user.is_superuser:
        # Aquí implementa la lógica para el panel de administrador
        return render(request, 'admin_panel.html')
    else:
        return HttpResponseForbidden("Acceso denegado. Debes ser un administrador para ver esta página.")

# Vista para el deslogueo del usuario
def logout_view(request):
    logout(request)
    return redirect('login')  # Redirige a la página de inicio de sesión después del logout
    
# Vista para el registro de usuario
def registro_view(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
        else:
            # Agregar mensajes de error al contexto
            for field, errors in form.errors.items():
                for error in errors:
                    messages.error(request, f"{field}: {error}")
    else:
        form = UserCreationForm()
    return render(request, 'registro.html', {'form': form})

# Vista para el inicio de sesión utilizando la vista de LoginView proporcionada por Django
def login_view(request):
    return LoginView.as_view()(request)

