"""
URL configuration for proyecto_dc_hector project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from decharla import views
from django.contrib.auth.views import LoginView

urlpatterns = [
    # Ruta para la página de inicio
    path('', views.home, name='home'),

    # Ruta para hacer login
    path('accounts/login/', LoginView.as_view(template_name='login.html'), name='login'),
    # ruta lista de camaras
    path('cameras/', views.camera_list, name='camera_list'),
    # ruta para la camara estatica
    path('cameras/<str:id>/', views.camera_detail, name='camera_detail'),
    # ruta para la camara dinamica
    path('cameras/<str:id>-dyn', views.camera_image, name='camera_image'),
    # ruta para los comentarios
    path('comentario/', views.camera_comments, name='camera_comments'),
    
    # ruta para actualizar los comentarios
    path('cameras/<str:camera_id>/comments/', views.get_comments_json, name='get_comments_json'),
    
    # Ruta para hacer registro
    path('accounts/registro/', views.registro_view, name='registro'),
    
    # Ruta para el logout
    path('logout/', views.logout_view, name='logout'),
    
    # Rutas adicionales para el menú horizontal
    path('configuration/', views.configuration, name='configuration'),
    path('help/', views.help_page, name='help'),
    path('admin/', views.admin_panel, name='admin'),
]
